# drunk_player

## Description

blabla 

- item 1
- item 2

## Dependances

- opencv
- boost 

## Utilisation

``` bash
./drunk_player_gui.out ../data
```

![](drunk_player_gui.png)

[dépot gitlab](https://gitlab.com/Jhermilier/L3_GL_etudiant.git)

lien explicite: <https://gitlab.com/Jhermilier/L3_GL_etudiant.git>

code inline ` int main() {return 0;}`

*italique*

**gras**

> bloc
> de code

- item 
- item
	- sous-item
	- sous-item
