#include "Fibo.hpp"
#include <iostream>
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, test_fibo_0) {
    int result = fibo(0);
    CHECK_EQUAL(0, result);
}

TEST(GroupFibo, test_fibo_1) {
    int result = fibo(1);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibo, test_fibo_2) {
    int result = fibo(2);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibo, test_fibo_3) {  
    int result = fibo(3);
    CHECK_EQUAL(2, result);
}

TEST(GroupFibo, test_fibo_4) { 
    int result = fibo(4);
    CHECK_EQUAL(3, result);
}

TEST(GroupFibo, test_throw_1) { 
	try{
		int a=fibo(-1);
	}catch (std::string & s)
	CHECK_EQUAL("f0 negatif", s);
}

TEST(GroupFibo, test_throw_2) { 
	try{
		int a=fibo(50);
	}catch (std::string & s)
	CHECK_EQUAL("f1 plus petit que f0", s);
}

