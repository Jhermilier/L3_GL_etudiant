#include "Spline.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupSpline) { };

TEST(GroupSpline, test_spline_0) {
    Spline s;
	s.addKey(0,{-1,0});
	s.addKey(1,{0,0});
	s.addKey(2,{1,1});
	s.addKey(3,{1,0});

    CHECK_EQUAL(1, s.getStartTime(),1e-6);
    CHECK_EQUAL(2, s.getEndTime(),1e-6);
}

TEST(GroupSpline, test_spline_1) {
    Spline s;
	s.addKey(0,{-1,0});
	s.addKey(1,{0,0});
	s.addKey(2,{1,1});
	s.addKey(3,{1,0});
	Vec2 v= s.getValue(1.5);
    CHECK_EQUAL(0, v.x_,1e-6);
    CHECK_EQUAL(1.202254, v.y_,1e-6);
}



