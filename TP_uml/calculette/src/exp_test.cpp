#include <iostream>
#include "exp.h"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupExp) { };

//VAL
TEST(GroupExp, Exprval_1)  {
    ExprVal expr(42);
    int res= expr.eval();
    CHECK_EQUAL(res, 42);
}
TEST(GroupExp, Exprval_2)  {
    ExprVal expr(0);
    int res= expr.eval();
    CHECK_EQUAL(res, 0);
}
TEST(GroupExp, Exprval_3)  {
    ExprVal expr(42);
    std::string res= expr.toRpn();
    CHECK_EQUAL(res, "42");
}

//ADD
TEST(GroupExp, Expradd_1)  {
    ExprVal a (21);
    ExprVal b(21);
    ExprAdd expr(&a,&b);
    int res= expr.eval();
    CHECK_EQUAL(res, 42);
}
TEST(GroupExp, Expradd_2)  {
    ExprVal a (21);
    ExprVal b (11);
    ExprVal c (10);
    ExprAdd expr1(&b,&c);
    ExprAdd expr2(&a,&expr1);
    int res1 =  expr1.eval();
    int res2 = expr2.eval();
    CHECK_EQUAL(res1, 21);
    CHECK_EQUAL(res2, 42);
}
TEST(GroupExp, Expradd_3)  {
    ExprVal a (32);
    ExprVal b (10);
    ExprAdd expr(&a,&b);
    std::string res = expr.toRpn();
    CHECK_EQUAL(res,"+ 32 10" );
}
TEST(GroupExp, Expradd_4)  {
    ExprVal a (2);
    ExprVal b (20);
    ExprVal c (1);
    ExprAdd expr1(&b,&c);
    ExprAdd expr2(&a,&expr1);
    std::string res = expr2.toRpn();
    CHECK_EQUAL(res,"+ 2 + 20 1" );
}

//MUL
TEST(GroupExp, Exprmul_1)  {
    ExprVal a (2);
    ExprVal b(10);
    ExprMul expr(&a,&b);
    int res= expr.eval();
    CHECK_EQUAL(res, 20);
}
TEST(GroupExp, Exprmul_2)  {
    ExprVal a (4);
    ExprVal b (2);
    ExprVal c (5);
    ExprMul expr1(&b,&c);
    ExprMul expr2(&a,&expr1);
    int res1 = expr1.eval();
    int res2 = expr2.eval();
    CHECK_EQUAL(res1, 10);
    CHECK_EQUAL(res2, 40);
}
TEST(GroupExp, Exprmul_3)  {
    ExprVal a (2);
    ExprVal b (20);
    ExprVal c (1);
    ExprAdd expr1(&b,&c);
    ExprMul expr2(&a,&expr1);
    int res1 = expr1.eval();
    int res2 = expr2.eval();
    CHECK_EQUAL(res1, 21);
    CHECK_EQUAL(res2, 42);
}
TEST(GroupExp, Exprmul_4)  {
    ExprVal a (2);
    ExprVal b (21);
    ExprMul expr(&a,&b);
    std::string res = expr.toRpn();
    CHECK_EQUAL(res,"* 2 21" );
}
TEST(GroupExp, Exprmul_5)  {
    ExprVal a (2);
    ExprVal b (20);
    ExprVal c (1);
    ExprMul expr1(&b,&c);
    ExprMul expr2(&a,&expr1);
    std::string res = expr2.toRpn();
    CHECK_EQUAL(res,"* 2 * 20 1" );
}
TEST(GroupExp, Exprmul_6)  {
    ExprVal a (2);
    ExprVal b (20);
    ExprVal c (1);
    ExprAdd expr1(&b,&c);
    ExprMul expr2(&a,&expr1);
    std::string res = expr2.toRpn();
    CHECK_EQUAL(res,"* 2 + 20 1" );
}

//parseExpr
TEST(GroupExp, parseExpr_1)  {
    std::istringstream iss ("+ 20 22");
    Expr* e= parseExpr(iss);
    CHECK_EQUAL(e->eval(),42 );
    CHECK_EQUAL(e->toRpn(),"+ 20 22" );
    delete e;
}
