#include <iostream>
#include <sstream>

#include "exp.h"

int main(){
    std::istringstream iss("* 2 + 11 10");
    Expr * e = parseExpr(iss);
    std::cout<< "eval: "<<e->eval()<<std::endl;
    std::cout<< "rpm: "<<e->toRpn()<<std::endl;
    delete e;
    return 0;
}
